# Projet13_Appli

Description
=========

Le but de ce projet est de pouvoir deployer une application écrite en PHP et qui puisse dialoguer avec une base de données mySQL dans Kubernetes.    

Requirements
------------

- Disposer des accès necessaire sur le cluster K8S
- Disposer des images docker php et mysql du projet
- Avoir déclaré le cluster Kubernetes dans gitlab dans la partie "Infrastructure" du projet :

![cluster Kubernetes](img/clusterK8S.png)


Dependencies
------------

Ce projet est divisé en 3 parties :

- le code php en lui-même présent dans le répertoire php. Dans ce repertoire, les sources sont présentes ainsi que le fichier Dockerfile qui permet de containeriser notre application.
- le repertoire "monApp" contenant tous les fichier YAML necessaire au deploiement de l'application dans Kubernetes et structuré de la façon suivante :
  - ingress.yml : gère l'accès externe aux services dans un cluster avec NGINX en tant que ingress controller
  - service.yml : permet d'acceder aux différents pods du projet. Nous avons utiliser 2 types de service :
      * NodePort : nous permets de rendre public notre pod "PHP"
      * Cluster : pour une exposition interne uniquement sur notre pod "MySQL"
  - secret.yml : permets la récuperation de notre image situé dans notre registry privé gitlab
  - StatefullSet.yml : permets la création de notre base de donnée MySQL
  - deployPHP.yml : permets la création de notre application en PHP
  - kustomization.yml : nous permets de deployer tous nos scripts de deploiement

- Le yaml permettant de générer notre pipeline dans gitlab afin qe le deploiement de l'application puisse se faire de manière automatique à chaque push.

Lancement du deploiement
----------------

```sh
kubectl apply -k monApp/
```